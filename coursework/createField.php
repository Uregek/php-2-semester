<?php
    $my_db = new mysqli('localhost', 'root', 'root', 'coursework');
    if (mysqli_connect_errno()) {
        echo '<div class="alert alert-danger d-flex align-items-center" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <div>
                    Ошибка при соединении с БД
                  </div>
              </div>';
    }

    if (isset($_POST['name']) && isset($_POST['description'])) {
        $sql = 'INSERT INTO Field(`name`, `description`) 
                VALUES ("'.trim(htmlspecialchars($_POST['name'])).'","'.trim(htmlspecialchars($_POST['description'])).'")';

        $sql_res = mysqli_query($my_db, $sql);
        if (mysqli_errno($my_db)) {
            echo '<div class="alert alert-danger d-flex align-items-center" role="alert">
                      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                      <div>
                        Ошибка при создании
                      </div>
                  </div>';
        } else {
            echo '<div class="alert alert-success d-flex align-items-center" role="alert">
                      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                      <div>
                        Успешно создано
                      </div>
                  </div>';
        }
    }
?>


<div style="margin: 50px; max-width: 500px">
    <form method="post" action="index.php?view=create">
        <div class="mb-3">
            <label for="name" class="form-label">Название области знаний</label>
            <input type="text" class="form-control" id="name" name="name" required autocomplete="off">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Описание</label>
            <input type="text" class="form-control" id="description" name="description" required autocomplete="off">
        </div>
        <button type="submit" class="btn btn-dark">Создать</button>
    </form>
</div>