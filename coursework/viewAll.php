<?php
    $my_db = new mysqli('localhost', 'root', 'root', 'coursework');
    if (mysqli_connect_errno()) {
        echo '<div class="alert alert-danger d-flex align-items-center" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <div>
                    Ошибка при соединении с БД
                  </div>
              </div>';
    }

    if (isset($_POST['field_id']) && isset($_POST['element'])) {
        $sql = 'INSERT INTO `#_to_Field`(`#_id`, `field_id`) 
                VALUES ("'.htmlspecialchars($_POST['element']).'","'.htmlspecialchars($_POST['field_id']).'")';

        $sql_res = mysqli_query($my_db, $sql);
        if (mysqli_errno($my_db)) {
            echo '<div class="alert alert-danger d-flex align-items-center" role="alert">
                      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                      <div>
                        Ошибка при присвоении
                      </div>
                   </div>';
        }
        else {
            echo '<div class="alert alert-success d-flex align-items-center" role="alert">
                      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                      <div>
                        Успешно присвоено
                      </div>
                   </div>';
        }
    }

    $sql = 'SELECT * FROM `#`';
    $sql_res = mysqli_query($my_db, $sql);
    if (mysqli_num_rows($sql_res) == 0) {
        echo '<div class="alert alert-danger d-flex align-items-center" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <div>
                    Записей нет
                  </div>
              </div>';
    }

    $rows = '';
    while ($row = mysqli_fetch_assoc($sql_res)) {
        $rows .= "<a href='?view=viewAll&element={$row['id']}' class='list-group-item list-group-item-action list-group-item-dark";
        if (isset($_GET["element"]) && $_GET["element"] == $row['id']) {
            $rows .= ' active ';
        }
        $rows .=  "'>{$row['name']}</a>";
    }

    if (isset($_GET['element'])) {
        $end = false;
        $sql = 'SELECT `name`, `id` FROM `Field` WHERE `id` NOT IN (SELECT `field_id` FROM `#_to_Field` WHERE `#_id`='.$_GET['element'].')';
        $sql_res = mysqli_query($my_db, $sql);
        if (mysqli_num_rows($sql_res) == 0) {
            $end = true;
        }
        $options = '';
        while ($row = mysqli_fetch_assoc($sql_res)) {
            $options .= "<option value='{$row['id']}'>{$row['name']}</option>";
        }
    }
?>


<div class="list-group" style="margin: 50px; max-width: 400px">
    <?= $rows ?>
</div>

<form method="post" action="index.php?view=viewAll" style="display: <?php if (!isset($_GET['element']) || $end) echo 'none'; ?>;">
    <input name="element" value="<?php if (isset($_GET['element'])) echo $_GET['element']; ?>" style="display: none">
    <select name="field_id" class="form-select" style="margin: 20px 20px 20px 50px; max-width: 400px; max-height: 50px" required>
        <?= $options ?>
    </select>
    <button type="submit" class="btn btn-dark" style="margin-left: 50px">Присвоить область знаний</button>
</form>

<?php
    if ($end) {
        echo '<div class="alert alert-warning d-flex align-items-center" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <div>
                    Выбранный элемент присвоен всем областям заний..
                  </div>
              </div>';
    }
?>


