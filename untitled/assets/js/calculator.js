
const textField = document.getElementById('textField');

textField.addEventListener('keydown', (e) => e.preventDefault());

const buttons = document.querySelectorAll('.calculator__button');

let history = [];

buttons.forEach((button) => {
    button.addEventListener('click', (e) => {
        e.preventDefault();

        switch (button.value) {
            case 'delete last': {
                if (history.length) {
                    history.pop();
                    textField.value = history[history.length - 1] || '';
                }
                break;
            }
            case 'clear': {
                textField.value = '';
                history = [];
                break;
            }
            default: {
                textField.value += e.target.value;
                history.push(textField.value);
            }
        }
    });
});