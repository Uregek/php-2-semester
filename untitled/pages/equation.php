<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="../assets/img/mos-logo.png" />
    <link rel="stylesheet" href="../assets/style.css">
    <title>Document</title>
</head>
<body>
<h1 class="visually-hidden">h1?</h1>

<header class="header">
    <div class="header__container container">
        <a class="header__link" href="index.php">
            <img class="header__logo" src="../assets/img/mos-logo.png" alt="logo" height="40" width="40">
        </a>
        <p class="header__text">Equation</p>
    </div>
</header>

<main class="main">

    <section class="dynamic-content section">
        <div class="dynamic-content__container container-m">

            <?php

            $equation = 'X * 9 = 56';

            function toAnswer($a, $b, $op) {
                if ($op === '+') {
                    return (int)$a + (int)$b;
                } else if ($op === '-') {
                    return (int)$a - (int)$b;
                } else if ($op === '*') {
                    return (int)$a * (int)$b;
                } else if ($op === '/') {
                    return (int)$a / (int)$b;
                }
            }

            function changeOperator($op) {
                if ($op === '+') {
                    return '-';
                } else if ($op === '-') {
                    return '+';
                } else if ($op === '*') {
                    return '/';
                } else if ($op === '/') {
                    return '*';
                }
            }

            $normalizedEquation = str_replace(' ', '', $equation);

            [$expression, $result] = explode('=', $normalizedEquation);

            preg_match('/(-?+(\d+|[a-zA-Z]+))([+\-*\/])(-?+(\d+|[a-z]+))/', $expression, $matches);
            [, $a, , $op, $b] = $matches;

            if (is_numeric($a) && is_numeric($b)) {
                echo 'Answer: '.$result.' = '.toAnswer($a, $b, $op);
            } else if (is_numeric($a) && is_numeric($result)) {
                $op = changeOperator($op);
                echo 'Answer: '.$b.' = '.toAnswer($result, $a, $op);
            } else if (is_numeric($b) && is_numeric($result)) {
                $op = changeOperator($op);
                echo 'Answer: '.$a.' = '.toAnswer($result, $b, $op);
            }

            ?>

        </div>
    </section>

</main>

<footer class="footer">
    <p class="footer__copyright">Copyright | 2022</p>
</footer>

</body>
</html>
