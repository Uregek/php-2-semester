<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="../assets/img/mos-logo.png" />
    <link rel="stylesheet" href="../assets/style.css">
    <title>Document</title>
</head>
<body>
    <h1 class="visually-hidden">h1?</h1>

    <header class="header">
        <div class="header__container container">
            <a class="header__link" href="index.php">
                <img class="header__logo" src="../assets/img/mos-logo.png" alt="logo" height="40" width="40">
            </a>
            <p class="header__text">Feedback Form</p>
        </div>
    </header>

    <main class="main">

        <section class="feedback section">
            <div class="feedback__container container-m">
                <h2 class="feedback__title">Результат работы функции get_headers:</h2>
                <textarea class="feedback__message" name="message" rows="10"><?php var_dump(get_headers('https://www.google.com'));?>
                </textarea>
            </div>
        </section>

    </main>

    <footer class="footer">
        <p class="footer__copyright">Copyright | 2022</p>
    </footer>

</body>
</html>
