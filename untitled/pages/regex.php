<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="../assets/img/mos-logo.png" />
    <link rel="stylesheet" href="../assets/style.css">
    <title>Document</title>
</head>
<body>
    <h1 class="visually-hidden">h1?</h1>

    <header class="header">
        <div class="header__container container">
            <a class="header__link" href="index.php">
                <img class="header__logo" src="../assets/img/mos-logo.png" alt="logo" height="40" width="40">
            </a>
            <p class="header__text">Regex</p>
        </div>
    </header>

    <main class="main">

        <section class="dynamic-content section">
            <div class="dynamic-content__container container-m">

                <?php
                    // 6
                    $str = 'aa aba abba abbba abca abea';
                    preg_match_all('/ab?a/', $str, $match);
                    var_dump($match[0]);

                    echo '</br>';

                    // 7
                    $str = 'ab abab abab abababab abea';
                    preg_match_all('/(ab)+/', $str, $match);
                    var_dump($match[0]);

                    echo '</br>';

                    // 8
                    $str = 'a.a aba aea';
                    preg_match_all('/a\.a/', $str, $match);
                    var_dump($match[0]);

                    echo '</br>';

                    // 9
                    $str = 'xbx aca aea abba adca abea';
                    echo preg_replace('/\b/', '!', $str);

                    echo '</br>';

                    $str = '123 4 11';
                    echo preg_replace_callback('/\d+/', fn($a) => $a[0] * $a[0], $str);
                ?>


            </div>
        </section>

    </main>

    <footer class="footer">
        <p class="footer__copyright">Copyright | 2022</p>
    </footer>

</body>
</html>