<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="../assets/img/mos-logo.png" />
    <link rel="stylesheet" href="../assets/style.css">
    <title>Document</title>
</head>
<body>
    <h1 class="visually-hidden">h1?</h1>

    <header class="header">
        <div class="header__container container">
            <a class="header__link" href="index.php">
                <img class="header__logo" src="../assets/img/mos-logo.png" alt="logo" height="40" width="40">
            </a>
            <p class="header__text">Feedback Form</p>
        </div>
    </header>

    <main class="main">

        <section class="feedback section">
            <div class="feedback__container container-m">
                <h2 class="feedback__title">Форма обратной связи</h2>
                <form action="https://httpbin.org/post" method="POST" class="feedback__form">
                    <input type="text" class="feedback__input input input_text" placeholder="Введите имя пользователя" name="user_name" required>
                    <input type="email" class="feedback__input input input_email" placeholder="Введите email" name="user_email" required>

                    <p class="feedback__text">Выберите тип обращения:</p>
                    <div class="feedback__type-of-appeal-box">
                        <label class="feedback__label-for-radio">
                            <input type="radio" name="appeal_type" value="complaint" required>
                            Жалоба
                        </label>

                        <label class="feedback__label-for-radio">
                            <input type="radio" name="appeal_type" value="offer">
                            Предложение
                        </label>

                        <label class="feedback__label-for-radio">
                            <input type="radio" name="appeal_type" value="gratitude">
                            Благодарность
                        </label>
                    </div>

                    <textarea class="feedback__message" name="message" rows="10" placeholder="Ведите текст обращения" required></textarea>

                    <p class="feedback__text">Выберите вариант ответа:</p>
                    <div class="feedback__type-of-appeal-box">
                        <label class="feedback__label-for-radio">
                            <input type="checkbox" name="answer_type" value="sms">
                            смс
                        </label>

                        <label class="feedback__label-for-radio">
                            <input type="checkbox" name="answer_type" value="email">
                            email
                        </label>
                    </div>

                    <div class="feedback__buttons-container">
                        <button class="button button_submit button_left" type="submit">Отправить</button>
                        <a href="get-headers-result.php" class="button button_submit button_right">Другая страница</a>
                    </div>
                </form>
            </div>
        </section>

    </main>

    <footer class="footer">
        <p class="footer__copyright">Copyright | 2022</p>
    </footer>

</body>
</html>
