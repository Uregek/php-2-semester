<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="../assets/img/mos-logo.png" />
    <link rel="stylesheet" href="../assets/style.css">
    <title>Document</title>
</head>
<body>
    <h1 class="visually-hidden">h1?</h1>

    <header class="header">
        <div class="header__container container">
            <a class="header__link" href="index.php">
                <img class="header__logo" src="../assets/img/mos-logo.png" alt="logo" height="40" width="40">
            </a>
            <p class="header__text">Hello, World!</p>
        </div>
    </header>

    <main class="main">

        <section class="dynamic-content section">
            <div class="dynamic-content__container container-m">
                <form class="dynamic-content__form" action="index.php" method="POST">
                    <input class="input input_text input_left" placeholder="Введите текст который следует отобразить ниже" name="text" type="text">
                    <button class="button button_submit button_right" type="submit">Отправить</button>
                </form>

                <?php
                    if (!empty($_POST['text'])) {
                        $text = strip_tags($_POST['text']);
                ?>
                        <p class="dynamic-content__title">Вы ввели:</p>
                        <p class="dynamic-content__entered-text"><?=$text?></p>
                <?php
                    }
                ?>


            </div>
        </section>

    </main>
    
    <footer class="footer">
        <p class="footer__copyright">Copyright | 2022</p>
    </footer>

</body>
</html>