<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="../assets/img/mos-logo.png" />
    <link rel="stylesheet" href="../assets/style.css">
    <title>Calculator</title>
</head>
<body>
<h1 class="visually-hidden">h1?</h1>

<header class="header">
    <div class="header__container container">
        <a class="header__link" href="index.php">
            <img class="header__logo" src="../assets/img/mos-logo.png" alt="logo" height="40" width="40">
        </a>
        <p class="header__text">Calculator</p>
    </div>
</header>

<main class="main">

    <section class="dynamic-content section">
        <div class="dynamic-content__container container-m">

            <?php
                function getLastBracket($str) {

                }
            ?>

            <form class="calculator" method="post" action="calculator.php">
                <input autocomplete="off" class="calculator__input" type="text" name="text" id="textField" value="<?php echo '1'; ?>">
                <button type="submit" class="calculator__submit">enter</button>
                <button class="calculator__button" value="7">7</button>
                <button class="calculator__button" value="8">8</button>
                <button class="calculator__button" value="9">9</button>
                <button class="calculator__button" value=" / ">/</button>
                <button class="calculator__button" value="4">4</button>
                <button class="calculator__button" value="5">5</button>
                <button class="calculator__button" value="6">6</button>
                <button class="calculator__button" value=" * ">*</button>
                <button class="calculator__button" value="1">1</button>
                <button class="calculator__button" value="2">2</button>
                <button class="calculator__button" value="3">3</button>
                <button class="calculator__button" value=" - ">-</button>
                <button class="calculator__button" value="0">0</button>
                <button class="calculator__button" value=" ( ">(</button>
                <button class="calculator__button" value=" ) ">)</button>
                <button class="calculator__button" value=" + ">+</button>
                <button class="calculator__button calculator__button_span_2" value="delete last">delete last</button>
                <button class="calculator__button calculator__button_span_2" value="clear">clear</button>
            </form>
        </div>
    </section>

</main>

<footer class="footer">
    <p class="footer__copyright">Copyright | 2022</p>
</footer>

<script src="../assets/js/calculator.js"></script>
</body>
</html>