<?php foreach($comments as $comment):?>
    <div style="display: flex; flex-direction: row; justify-content: space-between">
        <p id="comment<?= $comment->getId()?>"><?= $comment->getText();?></p>
        <a href="/comments/<?= $comment->getId()?>/edit">Редактировать</a>
    </div>
    <p><?= $comment->getCreatedAt();?></p>
    <hr>
<?php endforeach;?>