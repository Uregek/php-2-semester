<?php
    namespace MyProject\Models\Comments;
    use MyProject\Models\ActiveRecordEntity;
    use MyProject\Models\Articles\Article;
    use MyProject\Models\Users\User;
    use MyProject\Services\Db;

class Comment extends ActiveRecordEntity{
        protected $text;
        protected $authorId;
        protected $articleId;
        protected $createdAt;

        public function getText(){
            return $this->text;
        }
        public function getAuthorId(){
            return $this->authorId;
        }
        public function getArticleId(){
            return $this->articleId;
        }
        public function getCreatedAt(){
            return $this->createdAt;
        }

        public function setText(string $text){
            $this->text = $text;
        }
        public function setAuthorId(User $author){
            $this->authorId = $author->id;
        }
        public function setArticleId(Article $article){
            $this->articleId = $article->id;
        }

        public static function findByArticle(int $id): array
        {
            $db = Db::getInstance();
            $sql = 'SELECT * FROM `'.static::getTableName().'` WHERE article_id = :id';
            $comments = $db->query($sql, [':id' => $id], static::class );
            return $comments;
        }

        public static function getTableName(): string{
            return 'comments';
        }
    }
?>
