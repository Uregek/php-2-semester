<?php

namespace MyProject\Controllers;

use MyProject\Models\Articles\Article;
use MyProject\Models\Comments\Comment;
use MyProject\Models\Users\User;
use MyProject\View\View;

class CommentController{
    private $view;
    private $db;

    public function __construct()
    {
        $this->view = new View(__DIR__ . '/../../../templates');
    }

    public function add(int $articleId): void
    {
        $author = User::getById(1);
        $article = Article::getById($articleId);
        if ($article === null){
            $this->view->renderHtml('errors/404.php', [], 404);
            return;
        }

        $comment = new Comment();
        $comment->setText($_POST['text']);
        $comment->setAuthorId($author);
        $comment->setArticleId($article);
        $comment->save();
        header('Location: '.'/article/'.$article->getId().'#comment'.$comment->getId());
    }

    public function edit(int $commentId): void
    {
        $comment = Comment::getById($commentId);
        if ($comment === null){
            $this->view->renderHtml('errors/404.php', [], 404);
            return;
        }
        if (isset($_POST['text'])) {
            $comment->setText($_POST['text']);
            $comment->save();
            header('Location: '.'/article/'.$comment->getArticleId().'#comment'.$comment->getId());
        } else {
            $this->view->renderHtml('comments/editForm.php', ['comment' => $comment]);
        }
    }

    public function delete(int $articleId): void
    {
        $article = Article::getById($articleId);
        if ($article === null) {
            $this->view->renderHtml('errors/404.php', [], 404);
            return;
        }
        $article->delete();
    }
}